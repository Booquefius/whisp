package org.brian.whisp.stage;

import javafx.stage.Stage;
import org.brian.whisp.StageController;
import org.brian.whisp.Stages;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.util.Vector;

public class StageTwo extends AStage {

    private Item attached;
    private Location lastLocation;

    private final int attemptsToRemove = 200;
    private int attempts = 0;

    public StageTwo(Item attached){

        super(Stages.TWO);
        this.attached = attached;

    }

    @Override
    public void update() {

        if(!checkForValidity(attached)) {
            //Remove from the MAP
            StageController.getInstance().untrack(attached);
            return;
        }

        Location currentLocation = attached.getLocation();
        if(lastLocation != null) {

            if(lastLocation.distance(currentLocation) <= 0.2) {

                //Move onto another stage
                StageController.getInstance().moveToNextStage(attached);
                return;

            }

        }

        lastLocation = currentLocation.clone();

        attempts++;
        if(attempts == attemptsToRemove){
            StageController.getInstance().untrack(attached);
        }
    }
}
