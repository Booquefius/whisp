package org.brian.whisp;

import org.apache.commons.math3.util.Pair;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Item;
import org.bukkit.util.Vector;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class AsyncUtils {

    private static Whisp whisp;
    public static void initialize(Whisp whisp2){
        whisp = whisp2;
    }

    public static Pair<Boolean, Boolean> checkMech(Item item){

        CompletableFuture<Pair<Boolean, Boolean>> future = new CompletableFuture<>();
        runSync(() -> {

            Location location = item.getLocation();
            boolean isInWater = location.getBlock().getRelative(BlockFace.UP).getType().name().contains("WATER");
            boolean isOnHopper = location.getBlock().getType() == Material.HOPPER;

            future.complete(new Pair<>(isOnHopper, isInWater));

        });

        try {
            return future.get(2, TimeUnit.SECONDS);
        } catch (Exception ex){
            ex.printStackTrace();
        }

        return null;
    }

    private static void runSync(Runnable runnable){
        Bukkit.getScheduler().runTask(whisp, runnable);
    }

    public static void shoot(Item item) {
        runSync(() -> {

            Vector shootVector = new Vector(0d, 0.2, 0d);
            item.setVelocity(shootVector);

        });
    }

    public static Material getUnder(Location location){
        CompletableFuture<Material> future = new CompletableFuture<>();
        runSync(() -> {

            future.complete(location.getBlock().getRelative(BlockFace.DOWN).getType());

        });

        try {
            return future.get(2, TimeUnit.SECONDS);
        } catch (Exception ex){
            ex.printStackTrace();
        }

        return null;
    }

}
