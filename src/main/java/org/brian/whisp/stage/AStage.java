package org.brian.whisp.stage;

import org.brian.whisp.Stages;
import org.bukkit.entity.Item;

public abstract class AStage {

    private Stages stage;

    public abstract void update();
    public AStage(Stages stage){
        this.stage = stage;
    }

    public Stages getStage() {
        return stage;
    }

    public boolean checkForValidity(Item item){
        return !item.isDead();
    }

}
