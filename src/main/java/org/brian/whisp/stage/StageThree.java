package org.brian.whisp.stage;

import org.brian.whisp.AsyncUtils;
import org.brian.whisp.StageController;
import org.brian.whisp.Stages;
import org.bukkit.Bukkit;
import org.bukkit.entity.Item;

public class StageThree extends AStage {

    private Item attached;

    public StageThree(Item attached){

        super(Stages.THREE);
        this.attached = attached;

    }

    @Override
    public void update() {

        if(!checkForValidity(attached)) {
            //Remove from the MAP
            StageController.getInstance().untrack(attached);
            return;
        }

        AsyncUtils.shoot(attached);
        StageController.getInstance().reset(attached);

    }
}
