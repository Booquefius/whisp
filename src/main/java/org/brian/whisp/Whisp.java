package org.brian.whisp;

import javafx.stage.Stage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Whisp extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {

        AsyncUtils.initialize(this);
        new StageController(this);
        Bukkit.getPluginManager().registerEvents(this, this);
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&eWhisp enabled!"));
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&eWhisp is made by Brian || OOP-778 > Support Discord https://discord.gg/Z9bjuVr"));

    }

    @EventHandler (priority = EventPriority.HIGHEST)
    public void onSpawn(EntitySpawnEvent event) {

        if(!(event.getEntity() instanceof Item)) return;
        if(event.isCancelled() || event.getEntity().isDead()) return;

        StageController.getInstance().track(((Item) event.getEntity()));

    }

    @Override
    public void onDisable() {
        StageController.getInstance().end();
    }
}
