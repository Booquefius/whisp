package org.brian.whisp;

import org.brian.whisp.stage.AStage;
import org.brian.whisp.stage.StageOne;
import org.brian.whisp.stage.StageThree;
import org.brian.whisp.stage.StageTwo;
import org.bukkit.Bukkit;
import org.bukkit.entity.Item;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.Map;

public class StageController {

    private Whisp whisp;
    private static StageController stageController;
    private Map<Item, AStage> tracked = new HashMap<>();
    private BukkitTask task;

    public StageController(Whisp whisp){
        stageController = this;
        this.whisp = whisp;

        task = new BukkitRunnable(){
            @Override
            public void run() {

                itemsCopy().values().forEach(AStage::update);

            }
        }.runTaskTimerAsynchronously(whisp, 0, 10);

    }

    public static StageController getInstance() {
        return stageController;
    }

    public void untrack(Item item){
        this.tracked.remove(item);
    }

    public void track(Item item){
        this.tracked.put(item, new StageOne(item));
    }

    public Map<Item, AStage> itemsCopy(){
        return new HashMap<>(tracked);
    }

    public void moveToNextStage(Item item){

        AStage nextStage = nextStage(item);
        tracked.remove(item);
        if(nextStage == null) return;
        tracked.put(item, nextStage);

    }

    public void reset(Item item){
        tracked.remove(item);
        tracked.put(item, new StageOne(item));
    }

    private AStage nextStage(Item item) {

        Stages stage = Stages.getById(itemsCopy().get(item).getStage().getId()+1);
        if(stage == null) return null;

        if(stage == Stages.TWO) return new StageTwo(item);
        else return new StageThree(item);

    }

    public void end(){
        if(task != null) task.cancel();
        stageController = null;
    }

}
